# boogaboo_disasm_msx
This is a disassembly of the Z80 MSX-1 code of the MSX-1 "BOOGA-BOO" game.

This was the first game I played when I was a child of probably eight years old.
My father and I went to the home of my uncle and he showed me that MSX-1 (a Sony HB-20P), a machine that amazed me at that moment, and still does today. The computer was a present from my uncle, and it also came with a game in a cartridge, "La Pulga" (as we call it in Spanish), or "Booga-boo".

La Pulga is considered one of the first games made in Spain, and it's a very well known work by Francisco Suárez and Francisco Portalo ("Paco & Paco". Book here: http://www.bugabootheflea.com/). However, the MSX version was wrote by two persons about whom we known almost nothing nowadays. A complete mistery that absolutely puzzels me. They only thing we know about the authors of the MSX version is the short notice that they showed in the game: "Quicksilva presents BOOGA-BOO. By Steve & Ann Haigh".

I guess the reason why I decompiled this game was simply because it was a way to make real again some moments that are only memories now. And it didn't disappoint at all! Discovering all the thoughts of its creators as I was deciphering each byte was simply as amazing as addictive.

I hope you retro-lovers will enjoy it too :)

Miguel

=======================

Compile it with: z80asm disassembly.asm

It should generate a a.bin file with the following SHA sum: 9a63f2a6064a8e28f7251f78c3e91672f5de5253.

If you want to create a CAS file, simply join the HEADER file with a.bin and rename it to BOOGA.CAS.

![](http://mcolom.perso.math.cnrs.fr/ext_images/pulga/gi_scenario.png)
